package org.utkuozdemir.rhinojsenginetest;


import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.FunctionObject;
import org.mozilla.javascript.Scriptable;

import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Utku on 10.9.2014.
 */
public class Launcher {
	public static void main(String[] args) throws Exception {
		Context context = Context.enter();
		Scriptable scope = context.initStandardObjects();

		passJavaListToJsTest(context, scope);

		getValueFromJsToJavaTest(context, scope);

		callJsFunctionFromJavaTest(context, scope);

		callJavaMethodFromJsTest(context, scope);

		runJsFromFileTest(context, scope);
	}

	private static void callJavaMethodFromJsTest(Context context, Scriptable scope) throws NoSuchMethodException {
		FunctionObject functionObject
				= new FunctionObject("javaStaticMethod", Launcher.class.getMethod("javaStaticMethod"), scope);
		scope.put("javaStaticMethod", scope, functionObject);

		String javaFunctionCallSCript = "javaStaticMethod();";
		context.evaluateString(scope, javaFunctionCallSCript, "js", 1, null);
	}

	private static void callJsFunctionFromJavaTest(Context context, Scriptable scope) {
		String jsFunction = "function multiply(number1, number2) {" +
				"return number1 * number2;" +
				"}";
		Function function = context.compileFunction(scope, jsFunction, "js", 1, null);
		Object result = function.call(context, scope, scope, new Object[]{6, 7});
		System.out.println(result);
	}

	private static void getValueFromJsToJavaTest(Context context, Scriptable scope) {
		String additionScript =
				"var number = 3 + 5;";

		context.evaluateString(scope, additionScript, "js", 1, null);
		Object number = scope.get("number", scope);
		System.out.println(number);
	}

	private static void passJavaListToJsTest(Context context, Scriptable scope) {
		List<String> myList = Arrays.asList("utku", "volkan");
		scope.put("myList", scope, myList);

		String listToJsScript =
				"var l = myList.toArray();" +
						"for (index in l) { " +
						"java.lang.System.out.print(l[index]);" +
						"if (index < l.length - 1) { " +
						"java.lang.System.out.print(', ');" +
						"}" +
						"}";
		context.evaluateString(scope, listToJsScript, "js", 1, null);
	}

	private static void runJsFromFileTest(Context context, Scriptable scope) throws Exception {
		context.evaluateReader(
				scope,
				new InputStreamReader(Launcher.class.getClassLoader().getResourceAsStream("script1.js")),
				"js",
				1,
				null
		);
		Object number = scope.get("str", scope);
		System.out.println(number);
	}

	@SuppressWarnings("unused")
	public static void javaStaticMethod() {
		System.out.println("This is a java static method!");
	}
}
